%{?mingw_package_header}

ExclusiveArch:  x86_64

Name:           mingw-stoken
Version:        0.92
Release:        3%{?dist}
Summary:        Token code generator compatible with RSA SecurID 128-bit (AES) token

License:        LGPLv2+
URL:            https://stoken.sf.net
Source0:        https://netcologne.dl.sourceforge.net/project/stoken/stoken-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  mingw32-filesystem
BuildRequires:  mingw32-gcc
BuildRequires:  mingw32-libxml2
BuildRequires:  mingw32-nettle

BuildRequires:  mingw64-filesystem
BuildRequires:  mingw64-gcc
BuildRequires:  mingw64-libxml2
BuildRequires:  mingw64-nettle

BuildRequires:  autoconf automake make

%description
Software Token for Linux/UNIXi is a token code generator compatible with RSA
SecurID 128-bit (AES) tokens. It is a hobbyist project, not affiliated with or
endorsed by RSA Security.

# Mingw32
%package -n mingw32-stoken
Summary:        Token code generator compatible with RSA SecurID 128-bit (AES) token

%description -n mingw32-stoken
STOKEN is an extremely fast loss-less compression algorithm, providing compression
speed at 400 MB/s per core, scalable with multi-core CPU. It also features
an extremely fast decoder, with speed in multiple GB/s per core, typically
reaching RAM speed limits on multi-core systems.

# Mingw64
%package -n mingw64-stoken
Summary:        Token code generator compatible with RSA SecurID 128-bit (AES) token

%description -n mingw64-stoken
STOKEN is an extremely fast loss-less compression algorithm, providing compression
speed at 400 MB/s per core, scalable with multi-core CPU. It also features
an extremely fast decoder, with speed in multiple GB/s per core, typically
reaching RAM speed limits on multi-core systems.

%{?mingw_debug_package}


%prep
%autosetup -n stoken-%{version}


%build
%mingw_configure --without-gtk
%mingw_make %{?_smp_mflags}


%install
%mingw_make_install DESTDIR=%{buildroot}

find %{buildroot} -name \*.exe -delete
find %{buildroot} -name libstoken.a -delete
find %{buildroot} -name libstoken.la -delete
rm -rf %{buildroot}%{mingw32_docdir}
rm -rf %{buildroot}%{mingw64_docdir}
rm -rf %{buildroot}%{mingw32_mandir}
rm -rf %{buildroot}%{mingw64_mandir}

# Win32
%files -n mingw32-stoken
%doc COPYING.LIB
%{mingw32_bindir}/libstoken*.dll
%{mingw32_includedir}/stoken*.h
%{mingw32_libdir}/libstoken*.dll.a
%{mingw32_libdir}/pkgconfig/stoken.pc

# Win64
%files -n mingw64-stoken
%doc COPYING.LIB
%{mingw64_bindir}/libstoken*.dll
%{mingw64_includedir}/stoken*.h
%{mingw64_libdir}/libstoken*.dll.a
%{mingw64_libdir}/pkgconfig/stoken.pc

%changelog
* Tue Apr 27 2021 David Woodhouse <dwmw2@infradead.org> - 0.92-3
- Rebuild for new rawhide libnettle/libhogweed

* Tue Apr 6 2021 David Woodhouse <dwmw2@infradead.org> - 0.92-2
- Add make to BuildRequires

* Mon Apr 27 2020 David Woodhouse <dwmw2@infradead.org> - 0.92-1
- Initial mingw package
