%{?mingw_package_header}

ExclusiveArch:  x86_64

Name:           mingw-oath-toolkit
Version:        2.6.11
Release:        1%{?dist}
Summary:        One-time password components

License:        LGPLv2+
URL:            https://www.nongnu.org/oath-toolkit/
Source0:        https://download.savannah.nongnu.org/releases/oath-toolkit/oath-toolkit-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  mingw32-filesystem
BuildRequires:  mingw32-gcc
BuildRequires:  mingw32-libxml2
BuildRequires:  mingw32-nettle

BuildRequires:  mingw64-filesystem
BuildRequires:  mingw64-gcc
BuildRequires:  mingw64-libxml2
BuildRequires:  mingw64-nettle

BuildRequires:  autoconf automake make

%description
The OATH Toolkit provide components for building one-time password
authentication systems. It contains shared libraries, command line tools and a
PAM module. Supported technologies include the event-based HOTP algorithm
(RFC4226) and the time-based TOTP algorithm (RFC6238). OATH stands for Open
AuTHentication, which is the organization that specify the algorithms. For
managing secret key files, the Portable Symmetric Key Container (PSKC) format
described in RFC6030 is supported.

# Mingw32
%package -n mingw32-liboath
Summary:        Library for OATH handling

%description -n mingw32-liboath
OATH stands for Open AuTHentication, which is the organization that
specify the algorithms. Supported technologies include the event-based
HOTP algorithm (RFC4226) and the time-based TOTP algorithm (RFC6238).

%package -n mingw32-libpskc
Summary:        Library for PSKC handling

%description -n mingw32-libpskc
Library for managing secret key files, the Portable Symmetric Key
Container (PSKC) format described in RFC6030 is supported.

# Mingw64
%package -n mingw64-liboath
Summary:        Library for OATH handling

%description -n mingw64-liboath
OATH stands for Open AuTHentication, which is the organization that
specify the algorithms. Supported technologies include the event-based
HOTP algorithm (RFC4226) and the time-based TOTP algorithm (RFC6238).

%package -n mingw64-libpskc
Summary:        Library for PSKC handling

%description -n mingw64-libpskc
Library for managing secret key files, the Portable Symmetric Key
Container (PSKC) format described in RFC6030 is supported.

%{?mingw_debug_package}


%prep
%autosetup -n oath-toolkit-%{version}


%build
%mingw_configure --without-gtk
%mingw_make %{?_smp_mflags}


%install
%mingw_make_install DESTDIR=%{buildroot}

find %{buildroot} -name \*.exe -delete
find %{buildroot} -name liboath.a -delete
find %{buildroot} -name liboath.la -delete
find %{buildroot} -name libpskc.a -delete
find %{buildroot} -name libpskc.la -delete
find %{buildroot} -name \*.cmake -delete
rm -rf %{buildroot}%{mingw32_docdir}
rm -rf %{buildroot}%{mingw64_docdir}
rm -rf %{buildroot}%{mingw32_mandir}
rm -rf %{buildroot}%{mingw64_mandir}

# Win32
%files -n mingw32-liboath
%doc liboath/COPYING
%{mingw32_bindir}/liboath*.dll
%{mingw32_bindir}/libpskc*.dll
%{mingw32_datadir}/xml/pskc/*.xml
%{mingw32_datadir}/xml/pskc/*.xsd
%{mingw32_includedir}/liboath/oath.h
%{mingw32_includedir}/pskc/*.h
%{mingw32_libdir}/liboath*.dll.a
%{mingw32_libdir}/libpskc*.dll.a
%{mingw32_libdir}/pkgconfig/liboath.pc
%{mingw32_libdir}/pkgconfig/libpskc.pc

# Win64
%files -n mingw64-liboath
%doc liboath/COPYING
%{mingw64_bindir}/liboath*.dll
%{mingw64_bindir}/libpskc*.dll
%{mingw64_datadir}/xml/pskc/*.xml
%{mingw64_datadir}/xml/pskc/*.xsd
%{mingw64_includedir}/liboath/oath.h
%{mingw64_includedir}/pskc/*.h
%{mingw64_libdir}/liboath*.dll.a
%{mingw64_libdir}/libpskc*.dll.a
%{mingw64_libdir}/pkgconfig/liboath.pc
%{mingw64_libdir}/pkgconfig/libpskc.pc

%changelog
* Sun Feb 04 2024 Dimitri Papadopoulos Orfanos - 2.6.11-1
- Update to 2.6.11

* Sat Dec 09 2023 Dimitri Papadopoulos Orfanos - 2.6.9-1
- Update to 2.6.9

* Mon Apr 27 2020 David Woodhouse <dwmw2@infradead.org> - 2.6.2-1
- Initial mingw package
