%{?mingw_package_header}

ExclusiveArch:  x86_64

Name:           mingw-spdlog
Version:        1.12.0
Release:        1%{?dist}
Summary:        Super fast C++ logging library

License:        MIT
URL:            https://github.com/gabime/spdlog/
Source0:        https://github.com/gabime/spdlog/archive/v%{version}/spdlog-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  mingw32-filesystem
BuildRequires:  mingw32-gcc-c++

BuildRequires:  mingw64-filesystem
BuildRequires:  mingw64-gcc-c++

BuildRequires:  cmake

%?mingw_debug_package

%description
This is a packaged version of the gabime/spdlog C++ logging
library available at Github.

# Mingw32
%package -n mingw32-spdlog
Summary:        Super fast C++ logging library

%description -n mingw32-spdlog
This is a packaged version of the gabime/spdlog C++ logging
library available at Github.

# Mingw64
%package -n mingw64-spdlog
Summary:        Super fast C++ logging library

%description -n mingw64-spdlog
This is a packaged version of the gabime/spdlog C++ logging
library available at Github.

#{?mingw_debug_package}


%prep
%autosetup -n spdlog-%{version}


%build
%mingw_cmake -DSPDLOG_WCHAR_FILENAMES=ON -DSPDLOG_BUILD_EXAMPLE=OFF
%mingw_make %{?_smp_mflags}


%install
%mingw_make_install DESTDIR=%{buildroot}

find %{buildroot} -name \*.exe -delete
find %{buildroot} -name libspdlog.la -delete
rm -rf %{buildroot}%{mingw32_docdir}
rm -rf %{buildroot}%{mingw64_docdir}
rm -rf %{buildroot}%{mingw32_mandir}
rm -rf %{buildroot}%{mingw64_mandir}

# Win32
%files -n mingw32-spdlog
%doc LICENSE
%{mingw32_libdir}/libspdlog.dll.a
%{mingw32_bindir}/libspdlog.dll
%{mingw32_includedir}/spdlog/
%{mingw32_libdir}/cmake/spdlog/
%{mingw32_libdir}/pkgconfig/spdlog.pc

# Win64
%files -n mingw64-spdlog
%doc LICENSE
%{mingw64_libdir}/libspdlog.dll.a
%{mingw64_bindir}/libspdlog.dll
%{mingw64_includedir}/spdlog/
%{mingw64_libdir}/cmake/spdlog/
%{mingw64_libdir}/pkgconfig/spdlog.pc

%changelog
* Sat Dec 09 2023 Dimitri Papadopoulos Orfanos - 1.12.0-1
- Update to 1.12.0

* Tue Apr 06 2021 David Woodhouse <dwmw2@infradead.org> - 1.8.5-2
- Enable SPDLOG_WCHAR_FILENAMES

* Tue Apr 06 2021 David Woodhouse <dwmw2@infradead.org> - 1.8.5-1
- Update to 1.8.5

* Tue Apr 28 2020 David Woodhouse <dwmw2@infradead.org> - 1.5.0-1
- Initial mingw package
